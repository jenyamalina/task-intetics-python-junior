from django.db import models
from django.contrib.postgres import fields as psql_fields


class SortingResult(models.Model):
    sort_choices = (
        ('Bubble', 'Bubble'),
        ('Merge', 'Merge'),
        ('Insertion', 'Insertion')
    )

    time = models.DurationField()
    algorithm = models.CharField(choices=sort_choices, max_length=9)
    unsorted_array = psql_fields.ArrayField(models.IntegerField())
    sorted_array = psql_fields.ArrayField(models.IntegerField())

    def __str__(self):
        return f'id:{self.id}, algo: {self.algorithm}, time: {self.time}'