from django.urls import path

from sorters.views import IndexView

urlpatterns = [
    path('', IndexView.as_view())
]