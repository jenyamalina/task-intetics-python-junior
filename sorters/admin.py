from django.contrib import admin

from sorters.models import SortingResult


@admin.register(SortingResult)
class SortingResultAdmin(admin.ModelAdmin):
    list_display = ['id', 'time', 'algorithm']
    list_filter = ['id', 'time', 'algorithm']
    search_fields = ['id', 'time', 'algorithm']

