from django import forms


class SortingFileForm(forms.Form):
    sort_choices = (
        ('Bubble', 'Bubble'),
        ('Merge', 'Merge'),
        ('Insertion', 'Insertion'),
    )

    file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.txt', 'class': 'form-control'})
    )
    sort_options = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        choices=sort_choices,
        required=True,
    )
