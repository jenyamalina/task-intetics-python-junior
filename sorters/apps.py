from django.apps import AppConfig


class SortersConfig(AppConfig):
    name = 'sorters'
