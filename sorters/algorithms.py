import time
from abc import abstractmethod, ABC
from datetime import timedelta


def timed_function(func):
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()

        span = timedelta(seconds=end_time - start_time)
        return {'time': span, 'result': result}
    return wrapper


class Sortable(ABC):
    @classmethod
    @abstractmethod
    def sort(cls, array):
        return sorted(array)

    @classmethod
    @timed_function
    def timed_sort(cls, *args, **kwargs):
        return cls.sort(*args, **kwargs)


class BubbleSort(Sortable):
    @classmethod
    def sort(cls, array):
        swapped = True
        while swapped:
            swapped = False
            for i in range(1, len(array)):
                if array[i - 1] > array[i]:
                    swapped = True
                    array[i - 1], array[i] = array[i], array[i - 1]
        return array


class MergeSort(Sortable):
    @staticmethod
    def _merge(right: list, left: list):
        merged = []
        ri = 0
        li = 0
        while len(right) != ri and len(left) != li:
            if right[ri] > left[li]:
                merged.append(left[li])
                li += 1
            else:
                merged.append(right[ri])
                ri += 1
        while len(right) != ri:
            merged.append(right[ri])
            ri += 1
        while len(left) != li:
            merged.append(left[li])
            li += 1
        return merged

    @classmethod
    def sort(cls, array):
        if len(array) < 2:
            return array
        middle = len(array) // 2
        left = cls.sort(array[:middle])
        right = cls.sort(array[middle:])
        return cls._merge(left, right)


class InsertionSort(Sortable):
    @classmethod
    def sort(cls, array):
        for i in range(1, len(array)):
            element = array[i]
            i -= 1
            while array[i] > element and i >= 0:
                array[i + 1] = array[i]
                i -= 1
            array[i + 1] = element
        return array
