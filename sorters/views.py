from django.shortcuts import render

from django.views import View

from sorters.algorithms import BubbleSort, InsertionSort, MergeSort
from sorters.forms import SortingFileForm
from sorters.models import SortingResult


class IndexView(View):
    template_name = 'index.html'
    sorting_algorithms = (
        {
            'Bubble': BubbleSort,
            'Merge': MergeSort,
            'Insertion': InsertionSort,
         }
    )

    def post(self, request):
        form = SortingFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_data = form.files.get('file').read()
            try:
                integers = [int(element) for element in file_data.decode("utf-8").strip().split(' ')]
                unsorted = integers.copy()

                sort_algorithm_code = form.data.get('sort_options')
                timed_sorted_list = self.sorting_algorithms[sort_algorithm_code].timed_sort(integers)

                result = SortingResult.objects.create(time=timed_sorted_list.get('time'),
                                       algorithm=sort_algorithm_code,
                                       unsorted_array=unsorted,
                                       sorted_array=timed_sorted_list.get('result'))
                return render(request, self.template_name, {'form': form, 'sort_result': result})
            except ValueError:
                form.add_error('file', 'Invalid file structure')
                return render(request, self.template_name, {'form': form})
        return render(request, self.template_name, {'form': form})

    def get(self, request):
        form = SortingFileForm()
        return render(request, self.template_name, {'form': form})
