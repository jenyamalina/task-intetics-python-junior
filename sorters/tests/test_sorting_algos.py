import random

from django.test import TestCase

from sorters.algorithms import BubbleSort, MergeSort, InsertionSort


def get_random_list(start=-100, finish=100, size=None):
    size = random.randint(5, 20) if not size else size
    return [random.randint(start, finish) for _ in range(size)]


class BubbleTestCase(TestCase):
    random_seed = 1
    fixed_cases = (
        [1, 5, 3, 4, 7, 10, -2, -6, 34, 5],
        [-23, 65, -15, -22, 55, 45, -25, 20, 48, 77, -81, 78, 8, -13],
        [-5, 65, 76, -16, -79, 86, -42, 1, -29, -71, -83, -55, 48, -85, -53, -11, -95, 31, 60, -90],
        [25, -16, 73, -2, -60, 1, 45, -22, -100, -83],
        [35, 34, -55, 3, -43, 28, -33, 28, -46, -96, -77, -15, -78, 70, 38, 57, 39, 5, -35],
    )

    def test_fixed_cases(self):
        for test_case in self.fixed_cases:
            self.assertListEqual(BubbleSort.sort(test_case), sorted(test_case))

    def test_random_cases(self):
        random.seed(self.random_seed)

        for _ in range(100):
            test_case = get_random_list()
            self.assertListEqual(BubbleSort.sort(test_case), sorted(test_case))

    def test_timed_sort(self):
        for test_case in self.fixed_cases:
            func_ret = InsertionSort.timed_sort(test_case)
            assert 'time' in func_ret
            assert 'result' in func_ret
            self.assertListEqual(func_ret['result'], sorted(test_case))


class MergeSortTestCase(TestCase):
    random_seed = 1
    fixed_cases = (
        [1, 5, 3, 4, 7, 10, -2, -6, 34, 5],
        [-23, 65, -15, -22, 55, 45, -25, 20, 48, 77, -81, 78, 8, -13],
        [-5, 65, 76, -16, -79, 86, -42, 1, -29, -71, -83, -55, 48, -85, -53, -11, -95, 31, 60, -90],
        [25, -16, 73, -2, -60, 1, 45, -22, -100, -83],
        [35, 34, -55, 3, -43, 28, -33, 28, -46, -96, -77, -15, -78, 70, 38, 57, 39, 5, -35],
    )

    def test_fixed_cases_sort(self):
        for test_case in self.fixed_cases:
            self.assertListEqual(MergeSort.sort(test_case), sorted(test_case))

    def test_random_cases_sort(self):
        random.seed(self.random_seed)

        for _ in range(100):
            test_case = get_random_list()
            self.assertListEqual(MergeSort.sort(test_case), sorted(test_case))

    def test_random_cases_merge(self):
        random.seed(self.random_seed)

        for _ in range(100):
            right = sorted(get_random_list())
            left = sorted(get_random_list())

            list_got = MergeSort._merge(right, left)
            list_expected = sorted(right + left)

            self.assertListEqual(list_got, list_expected)

    def test_timed_sort(self):
        for test_case in self.fixed_cases:
            func_ret = InsertionSort.timed_sort(test_case)
            assert 'time' in func_ret
            assert 'result' in func_ret
            self.assertListEqual(func_ret['result'], sorted(test_case))


class InsertionSortTestCase(TestCase):
    random_seed = 1
    fixed_cases = (
        [1, 5, 3, 4, 7, 10, -2, -6, 34, 5],
        [-23, 65, -15, -22, 55, 45, -25, 20, 48, 77, -81, 78, 8, -13],
        [-5, 65, 76, -16, -79, 86, -42, 1, -29, -71, -83, -55, 48, -85, -53, -11, -95, 31, 60, -90],
        [25, -16, 73, -2, -60, 1, 45, -22, -100, -83],
        [35, 34, -55, 3, -43, 28, -33, 28, -46, -96, -77, -15, -78, 70, 38, 57, 39, 5, -35],
    )

    def test_fixed_cases_sort(self):
        for test_case in self.fixed_cases:
            self.assertListEqual(InsertionSort.sort(test_case), sorted(test_case))

    def test_random_cases_sort(self):
        random.seed(self.random_seed)

        for _ in range(100):
            test_case = get_random_list()
            self.assertListEqual(InsertionSort.sort(test_case), sorted(test_case))

    def test_timed_sort(self):
        for test_case in self.fixed_cases:
            func_ret = InsertionSort.timed_sort(test_case)
            assert 'time' in func_ret
            assert 'result' in func_ret
            self.assertListEqual(func_ret['result'], sorted(test_case))
