# Intetics test task

Django/Server/Postgres: 

__user__: jenyamalina 

__password__: 123456

## Django part
#### What's done

##### What was asked
-  implemented a hierarchy of classes which includes 
a base abstract class with multiple descendant classes
implementing the functionality
- the clases implement methods demonstrating various 
sorting algorithms (Bubble, Insertion and Merge)
- created a decorator which measures execution time for a decorated function, use it to decorate the sorting
methods of classes
- set up a simple Django model representing executions of the algorithms
- added the model to the Django admin section, 
allowed filtering by algorithm type, 
and enabled search and sorting by any model field
- created a simple Django view containing a form 
which allows to choose an algorithm, 
pick a file with unsorted integers, 
and execute the sorting 
(with saving a record in the databse upon completion)


##### What wasn't asked but still done
- minor sorting algorithm testing
- base template


#### What could been done or I'm aware of
- full coverage testing (views/forms/etc)
- better UI


p.s. tests are intentionally not DRY

## Server part
- Link to the VM [https://yadi.sk/d/wzYp4qT8bmuIiQ](https://yadi.sk/d/wzYp4qT8bmuIiQ)
- Installed Nginx, PostgreSQL, Gunicorn 
and configured them to work with Django server
- VM should be on a local network with IP starting with 192.*
otherwise you need to change `server_name` to your IP in file `/etc/nginx/sites-available/cloudproject` 
- Code is located at `~/django/interview`

p.s. never set up nginx/gunicorn before... that turned out to be fun (of course with some pain in the *** 😅)